/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2017
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.opalj.br.cfg

/**
 * Represents the artificial exit node of a control flow graph. The graph contains
 * an explicit exit node to make it trivial to navigate to all instructions that may
 * cause a(n ab)normal return from the method.
 *
 * @author Erich Wittenbeck
 * @author Michael Eichberg
 */
class ExitNode( final val normalReturn: Boolean) extends CFGNode {

    final override def nodeId: Int = if (normalReturn) Int.MinValue else Int.MinValue + 1

    final override def isBasicBlock: Boolean = false
    final override def isCatchNode: Boolean = false
    final override def isExitNode: Boolean = true

    final override def isStartOfSubroutine: Boolean = false

    final override def addSuccessor(successor: CFGNode): Unit = {
        throw new UnsupportedOperationException()
    }

    final override private[cfg] def setSuccessors(successors: Set[CFGNode]): Unit = {
        throw new UnsupportedOperationException()
    }

    //
    // FOR DEBUGGING/VISUALIZATION PURPOSES
    //

    override def toString(): String = s"ExitNode(normalReturn=$normalReturn)"

    override def toHRR: Option[String] = {
        Some(if (normalReturn) "Normal Return" else "Abnormal Return")
    }

    override def visualProperties: Map[String, String] = {
        if (normalReturn)
            Map(
                "labelloc" → "l",
                "fillcolor" → "green",
                "style" → "filled"
            )
        else
            Map(
                "labelloc" → "l",
                "fillcolor" → "red",
                "style" → "filled",
                "shape" → "octagon"
            )
    }

}

object ExitNode {

    def unapply(en: ExitNode): Some[Boolean] = Some(en.normalReturn)
}
