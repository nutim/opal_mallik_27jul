/* BSD 2-Clause License:
 * Copyright (c) 2009 - 2017
 * Software Technology Group
 * Department of Computer Science
 * Technische Universität Darmstadt
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.opalj
package br

/**
 * Definition of a Java 9 module.
 *
 * @author Michael Eichberg
 */
case class Module(
        requires: IndexedSeq[Requires],
        exports:  IndexedSeq[Exports],
        uses:     IndexedSeq[ObjectType],
        provides: IndexedSeq[Provides]
) extends Attribute {

    final override def kindId: Int = Module.KindId

    // TODO Determine if the order is relevant or not and if not correct similar
    override def similar(other: Attribute): Boolean = this == other
}

object Module {

    final val KindId = 44

}

/**
 * @param requires The name of a required module.
 */
case class Requires(
    requires:      String,
    requiresFlags: Int
)

/**
 * @param   exports Name of an exported package in internal form.
 * @param   exportsTo List of names of modules whose code can access the
 *          public types in this exported package (in internal form).
 */
case class Exports(
    exports:   String,
    exportsTo: IndexedSeq[String]
)

case class Provides(
    provides:      ObjectType,
    withInterface: ObjectType
)
